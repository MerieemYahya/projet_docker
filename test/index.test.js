const app = require('../app')
const request = require('supertest')
const chai = require('chai'),
    expect = chai.expect 

var champ = {
    name:"testname",
    title:"testtitle",
    type:"testtype",  
   }

describe('JSON', () => {
    it('Render View with data', done => {
        request(app)
            .get('/json')
            .end((err, res) => {
                 expect(res.header['content-type']).to.equal('text/html; charset=utf-8');
                 expect(res.text).to.include("Rise of the Thorns")
                 done()          
            })
              
    })
     it('Render View inputs', done => {
        request(app)
            .get('/json/add')
            .end((err, res) => {
                 expect(res.header['content-type']).to.equal('text/html; charset=utf-8');
                 expect(res.text).to.include("Add champs")
                 done()          
            })
              
    })

    it('Add data to JSON', done => {
        request(app)
            .post('/json/add')
            .send(champ)           
            .end((err, res) => {
                expect(res.statusCode).to.equal(302);
                done()
            })
    })

})

